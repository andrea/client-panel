import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { NavLink as RouterNavLink } from 'react-router-dom'
import { NavItem, NavLink } from 'reactstrap'
import { LOGIN, REGISTER } from '../../routes'

const WithAuthentication = Component =>
  /**
   * @return {null}
   */
  function WithAuthenticationComponent({
    isAuthenticated,
    allowRegistration,
    ...props
  }) {
    if (!isAuthenticated && !allowRegistration) return null

    if (!isAuthenticated && allowRegistration) {
      return (
        <Fragment>
          <NavItem>
            <NavLink tag={RouterNavLink} to={LOGIN} activeClassName="active">
              Login
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink tag={RouterNavLink} to={REGISTER} activeClassName="active">
              Register
            </NavLink>
          </NavItem>
        </Fragment>
      )
    }

    return <Component {...props} />
  }

WithAuthentication.propTypes = {
  isAuthenticated: PropTypes.bool,
}

export default WithAuthentication
