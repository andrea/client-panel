/* eslint-disable security/detect-object-injection */
import validator from 'validator'

const validateNewClient = client => {
  const errors = {}
  // Define required fields
  // noinspection JSUnusedLocalSymbols
  const { phone, balance, ...required } = client

  // Check for all required input fields
  Object.keys(required).forEach(prop => {
    if (validator.isEmpty(client[prop].toString().trim())) {
      // prettier-ignore
      errors[prop] = `${prop[0].toUpperCase()}${prop.slice(1).toLowerCase()} field is required`
    }
  })

  // If required fields are empty return early
  if (Object.keys(errors).length > 0) return errors

  // Validate first name
  if (!validator.isLength(client.firstName, { min: 3, max: 64 })) {
    errors.firstName =
      'First name must be a string between 3 and 64 characters.'
  }

  // Validate last name
  if (!validator.isLength(client.lastName, { min: 3, max: 64 })) {
    errors.lastName =
      'Last name must be a string between 3 and 64 characters containing only letters.'
  }

  // Validate email
  if (!validator.isEmail(client.email)) {
    errors.email = 'Email is invalid.'
  }

  // Validate phone number
  if (!validator.isMobilePhone(client.phone)) {
    errors.phone = 'Phone number is invalid.'
  }

  // Validate balance
  if (
    !validator.isEmpty(client.balance.toString()) &&
    !validator.isDecimal(client.balance.toString())
  ) {
    errors.balance = 'Balance must be a valid integer or decimal number.'
  }

  return errors
}

const validateBalance = value => {
  let error = ''

  if (
    validator.isEmpty(value.toString()) ||
    !validator.isDecimal(value.toString())
  ) {
    error = 'Balance is invalid.'
  }

  return error
}

export { validateNewClient, validateBalance }
