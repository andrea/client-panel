import React from 'react'
import PropTypes from 'prop-types'
import {
  Card,
  CardFooter,
  CardHeader,
  Container,
  Jumbotron,
  Table,
} from 'reactstrap'
import ClientItem from '../ClientItem/ClientItem'

// Todo: WithClients component
const ClientList = ({ clients }) => (
  <Card>
    <CardHeader className="text-muted" />
    {clients.length > 0 ? (
      <Table responsive striped className="mb-0">
        <thead className="thead-dark">
          <tr>
            <th>Name</th>
            <th className="d-none d-sm-table-cell">Email</th>
            <th className="text-right">Balance</th>
            <th className="text-right" />
          </tr>
        </thead>
        <tbody>
          {clients.map(client => (
            <ClientItem client={client} key={client.id} />
          ))}
        </tbody>
      </Table>
    ) : (
      <Jumbotron fluid className="mb-0 bg-white">
        <Container fluid>
          <p className="font-weight-light text-center mb-0 font-italic text-secondary">
            Please, add at least one client&hellip;
          </p>
        </Container>
      </Jumbotron>
    )}
    <CardFooter className="text-muted" />
  </Card>
)

ClientList.propTypes = {
  clients: PropTypes.array,
}

export default ClientList
