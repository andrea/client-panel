import React from 'react'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Header = ({ icon, title, children }) => (
  <header className="d-flex justify-content-between align-items-center mb-2 pt-5">
    <h2 className="m-0 text-dark">
      <FontAwesomeIcon icon={icon} />
      <span className="ml-2">{title}</span>
    </h2>
    {children}
  </header>
)

Header.propTypes = {
  title: PropTypes.string,
  icon: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
}

Header.defaultProps = {
  title: 'Page Title',
}

export default Header
