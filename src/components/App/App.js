import React, { Fragment } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import store from '../../store'
import { UserIsAuthenticated, UserIsNotAuthenticated } from '../../helpers/auth'
import AppNavbar from '../AppNavbar/AppNavbar'
import Home from '../Home/Home'
import ClientAdd from '../ClientAdd/ClientAdd'
import ClientEdit from '../ClientEdit/ClientEdit'
import ClientPage from '../ClientPage/ClientPage'
import AppFooter from '../AppFooter/AppFooter'
import Login from '../Login/Login'
import AppSettingsPage from '../AppSettingsPage/AppSettingsPage'
import Register from '../Register/Register'
import NotFound from '../NotFound/NotFound'
import {
  ADD_CLIENT,
  CLIENT,
  EDIT_CLIENT,
  HOME,
  LOGIN,
  REGISTER,
  SETTINGS,
} from '../../routes'

const App = () => (
  <Router>
    <Provider store={store}>
      <Fragment>
        <AppNavbar />
        <Switch>
          <Route exact path={HOME} component={UserIsAuthenticated(Home)} />
          <Route
            exact
            path={SETTINGS}
            component={UserIsAuthenticated(AppSettingsPage)}
          />
          <Route
            exact
            path={ADD_CLIENT}
            component={UserIsAuthenticated(ClientAdd)}
          />
          <Route
            exact
            path={EDIT_CLIENT}
            component={UserIsAuthenticated(ClientEdit)}
          />
          <Route
            exact
            path={`${CLIENT}/:id`}
            component={UserIsAuthenticated(ClientPage)}
          />
          <Route exact path={LOGIN} component={UserIsNotAuthenticated(Login)} />
          <Route
            exact
            path={REGISTER}
            component={UserIsNotAuthenticated(Register)}
          />
          <Route component={NotFound} />
        </Switch>
        <AppFooter />
      </Fragment>
    </Provider>
  </Router>
)

export default App
