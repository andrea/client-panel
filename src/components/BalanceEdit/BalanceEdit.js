import React from 'react'
import PropTypes from 'prop-types'
import {
  Button,
  Form,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
} from 'reactstrap'

const BalanceEdit = ({ value, error, onSubmit, onChange }) => (
  <Form inline onSubmit={onSubmit} noValidate>
    <InputGroup>
      <Input
        type="number"
        name="balance"
        id="balance"
        value={value}
        onChange={onChange}
        bsSize="sm"
        invalid={!!error}
      />
      <InputGroupAddon addonType="append">
        <Button color="dark" size="sm" type="submit">
          Update
        </Button>
      </InputGroupAddon>
      <FormFeedback>{error}</FormFeedback>
    </InputGroup>
  </Form>
)

BalanceEdit.propTypes = {
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  error: PropTypes.string,
  onSubmit: PropTypes.func,
  onChange: PropTypes.func,
}

export default BalanceEdit
