import {
  DISABLE_BALANCE_ON_ADD,
  DISABLE_BALANCE_ON_EDIT,
  ALLOW_REGISTRATION,
} from './types'

const setDisabledBalanceOnAdd = () => {
  // Get settings from localStorage
  const settings = JSON.parse(localStorage.getItem('settings'))
  // Toggle setting
  settings.disableBalanceOnAdd = !settings.disableBalanceOnAdd
  // Set back to localStorage
  localStorage.setItem('settings', JSON.stringify(settings))

  return {
    type: DISABLE_BALANCE_ON_ADD,
    payload: {
      disableBalanceOnAdd: settings.disableBalanceOnAdd,
    },
  }
}

const setDisabledBalanceOnEdit = () => {
  // Get settings from localStorage
  const settings = JSON.parse(localStorage.getItem('settings'))
  // Toggle setting
  settings.disableBalanceOnEdit = !settings.disableBalanceOnEdit
  // Set back to localStorage
  localStorage.setItem('settings', JSON.stringify(settings))

  return {
    type: DISABLE_BALANCE_ON_EDIT,
    payload: {
      disableBalanceOnEdit: settings.disableBalanceOnEdit,
    },
  }
}

const setAllowRegistration = () => {
  // Get settings from localStorage
  const settings = JSON.parse(localStorage.getItem('settings'))
  // Toggle setting
  settings.allowRegistration = !settings.allowRegistration
  // Set back to localStorage
  localStorage.setItem('settings', JSON.stringify(settings))

  return {
    type: ALLOW_REGISTRATION,
    payload: {
      allowRegistration: settings.allowRegistration,
    },
  }
}

export {
  setDisabledBalanceOnAdd,
  setDisabledBalanceOnEdit,
  setAllowRegistration,
}
