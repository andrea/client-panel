import React from 'react'
import Loader from 'react-loader-spinner'
import { Container, Row } from 'reactstrap'

const Spinner = () => (
  <Container className="h-100">
    <Row className="h-100 justify-content-center align-items-center">
      <Loader type="TailSpin" color="#dadada" height="64" width="64" />
    </Row>
  </Container>
)

export default Spinner
