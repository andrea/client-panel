import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firebaseConnect } from 'react-redux-firebase'
import { NavLink as RouterNavLink } from 'react-router-dom'

import {
  Collapse,
  Container,
  Nav,
  Navbar,
  NavbarBrand,
  NavbarToggler,
} from 'reactstrap'
import NavLogin from '../NavLogin/NavLogin'
import WithAuthentication from '../WithAuthentication/WithAuthentication'
import { HOME } from '../../routes'

class AppNavbar extends React.Component {
  state = {
    isOpen: false,
    isAuthenticated: false,
  }

  // eslint-disable-next-line no-unused-vars
  componentDidUpdate(props, state, snapshot) {
    if (props.auth && props.auth.isEmpty !== this.props.auth.isEmpty) {
      this.setState(() => ({ isAuthenticated: !state.isAuthenticated }))
    }
  }

  handleToggle = () => {
    this.setState({
      isOpen: !this.state.isOpen,
    })
  }

  handleLogoutClick = e => {
    e.preventDefault()
    this.props.firebase.logout()
  }

  render() {
    const WithAuthenticationMenu = WithAuthentication(NavLogin)

    return (
      <Navbar color="dark" dark expand="md">
        <Container>
          <NavbarBrand tag={RouterNavLink} to={HOME} activeClassName="active">
            ClientPanel
          </NavbarBrand>
          <NavbarToggler onClick={this.handleToggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <WithAuthenticationMenu
                username={this.props.auth.email}
                isAuthenticated={this.state.isAuthenticated}
                handleLogoutClick={this.handleLogoutClick}
                allowRegistration={this.props.settings.allowRegistration}
              />
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    )
  }
}

AppNavbar.propTypes = {
  firebase: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  settings: PropTypes.object.isRequired,
}

const mapStateToProps = ({ firebase: { auth }, settings }) => ({
  auth,
  settings,
})

export default compose(
  firebaseConnect(),
  connect(mapStateToProps)
)(AppNavbar)
