import React from 'react'
import ReactDOM from 'react-dom'
import dotenv from 'dotenv'
import App from './components/App/App'
import fontAwesome from './fontAwesome/fontAwesome'
import 'bootstrap/dist/css/bootstrap.min.css'
import registerServiceWorker from './registerServiceWorker'

import './global.css'

dotenv.config()
fontAwesome()

ReactDOM.render(<App />, document.getElementById('root'))
registerServiceWorker()
