import React from 'react'
import PropTypes from 'prop-types'
import { NavLink as RouterNavLink } from 'react-router-dom'
import { Container, Nav, Navbar, NavItem, NavLink } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const AppNavbarSecondary = ({ route, title, children }) => (
  <Navbar color="light" light expand="md">
    <Container>
      <Nav className="d-flex w-100 justify-content-between" navbar>
        {route && (
          <NavLink
            tag={RouterNavLink}
            to={route}
            activeClassName="active"
            className="text-secondary mr-auto"
          >
            <FontAwesomeIcon icon="chevron-circle-left" />
            <span className="ml-2">{title}</span>
          </NavLink>
        )}
        {children && <NavItem className="ml-auto">{children}</NavItem>}
      </Nav>
    </Container>
  </Navbar>
)

AppNavbarSecondary.propTypes = {
  route: PropTypes.string,
  children: PropTypes.object,
  title: PropTypes.string,
}

export default AppNavbarSecondary
