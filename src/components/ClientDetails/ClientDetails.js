import React from 'react'
import PropTypes from 'prop-types'
import {
  Badge,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardSubtitle,
  CardTitle,
  ListGroup,
  ListGroupItem,
} from 'reactstrap'

const ClientDetails = ({ client }) => (
  <Card>
    <CardHeader className="text-muted" />
    <CardBody>
      <CardTitle>
        <div className="h2">{`${client.firstName} ${client.lastName}`}</div>
      </CardTitle>
      <CardSubtitle>
        <div className="h5">
          Client ID:
          <Badge color="light" className="text-monospace ml-2">
            {client.id}
          </Badge>
        </div>
      </CardSubtitle>
      <hr />
      <ListGroup>
        <ListGroupItem>Contact Email: {client.email}</ListGroupItem>
        <ListGroupItem>Contact Phone: {client.phone}</ListGroupItem>
      </ListGroup>
    </CardBody>
    <CardFooter className="text-muted" />
  </Card>
)

ClientDetails.propTypes = {
  client: PropTypes.object,
}

export default ClientDetails
