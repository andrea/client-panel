import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import { Col, Container, Row } from 'reactstrap'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import AppNavbarSecondary from '../AppNavbarSecondary/AppNavbarSecondary'
import ClientForm from '../ClientForm/ClientForm'
import Header from '../Header/Header'
import { HOME } from '../../routes'
import { validateNewClient } from '../../validation'

class ClientEdit extends Component {
  initialState = {
    shouldRedirect: false,
    isLoading: true,
    client: {
      id: '',
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      balance: 0,
    },
    errors: {},
  }

  state = { ...this.initialState }

  componentDidMount() {
    const { client, requesting } = this.props
    if (!requesting.clients && Object.keys(client).length === 0) {
      this.setState(() => ({ shouldRedirect: true }))
    }
    this.setState(() => ({ isLoading: false, client }))
    this.initialState.client = { ...client }
  }

  handleSubmit = async e => {
    e.preventDefault()
    const errors = validateNewClient(this.state.client)
    await this.setState(() => ({ errors }))
    // Update in firestore if necessary.
    if (
      Object.keys(this.state.errors).length === 0 &&
      this.state.client !== this.props.client
    ) {
      await this.props.firestore.update(
        { collection: 'clients', doc: this.state.client.id },
        this.state.client
      )
      this.setState(() => ({ shouldRedirect: true }))
    }
  }

  handleReset = () => {
    this.setState(() => ({ ...this.initialState }))
  }

  handleInputChange = e => {
    e.persist()
    this.setState(state => ({
      client: { ...state.client, [e.target.name]: e.target.value },
    }))
  }

  render() {
    if (this.state.shouldRedirect) {
      return <Redirect to={HOME} />
    }

    return (
      <Fragment>
        <AppNavbarSecondary route={HOME} title="Back to Clients" />
        <Container>
          <Row>
            <Col>
              <Header title="Edit Client" icon="user-circle" />
            </Col>
          </Row>
          <Row>
            <Col>
              <main className="mt-3">
                <ClientForm
                  client={this.state.client}
                  errors={this.state.errors}
                  handleSubmit={this.handleSubmit}
                  handleReset={this.handleReset}
                  handleInputChange={this.handleInputChange}
                  disableBalanceInput={this.props.settings.disableBalanceOnEdit}
                />
              </main>
            </Col>
          </Row>
        </Container>
      </Fragment>
    )
  }
}

ClientEdit.propTypes = {
  client: PropTypes.object,
  requesting: PropTypes.object,
  firestore: PropTypes.object,
  history: PropTypes.object,
  settings: PropTypes.object,
}

const mapStateToProps = ({
  firestore: {
    status: { requesting },
    ordered: { client },
  },
  settings,
}) => {
  if (!client) return { client: {}, requesting, settings }
  return { client: client[0], requesting, settings }
}

const firestoreQuery = props => [
  { collection: 'clients', storeAs: 'client', doc: props.match.params.id },
]

export default compose(
  firestoreConnect(firestoreQuery),
  connect(mapStateToProps)
)(ClientEdit)
