import React from 'react'
import { Col, Container, Row } from 'reactstrap'

const AppFooter = () => (
  <footer className="bg-light mt-5">
    <Container>
      <Row>
        <Col className="py-5 text-secondary bg-light">
          <p className="text-muted">
            Copyright &copy; {new Date().getFullYear()} ClientPanel. All rights
            reserved.
          </p>
        </Col>
      </Row>
    </Container>
  </footer>
)

export default AppFooter
