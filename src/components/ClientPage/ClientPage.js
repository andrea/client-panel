/* eslint-disable no-unused-vars */
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
// prettier-ignore
import { Col, Container, Popover, PopoverBody, PopoverHeader, Row } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import AppNavbarSecondary from '../AppNavbarSecondary/AppNavbarSecondary'
import Header from '../Header/Header'
import Balance from '../Balance/Balance'
import BalanceEdit from '../BalanceEdit/BalanceEdit'
import WithLoading from '../WithLoading/WithLoading'
import ClientDetails from '../ClientDetails/ClientDetails'
import ClientActionsEditDelete from '../ClientActionsEditDelete/ClientActionsEditDelete'
import { HOME } from '../../routes'
import { validateBalance } from '../../validation'

class ClientPage extends Component {
  state = {
    client: null,
    shouldRedirect: false,
    isLoading: true,
    popoverOpen: false,
    form: {
      balance: 0,
      error: '',
    },
  }

  // noinspection JSUnusedLocalSymbols
  componentDidUpdate(props, state, snapshot) {
    if (props.client !== this.props.client) {
      this.setState(prevState => ({
        isLoading: false,
        client: this.props.client,
        form: {
          ...prevState.form,
          balance: this.props.client ? this.props.client.balance : 0,
        },
      }))
    }
  }

  handlePopoverToggle = () => {
    this.setState(state => ({
      popoverOpen: !state.popoverOpen,
    }))
  }

  handleFormSubmit = async e => {
    e.preventDefault()

    const { client, firestore } = this.props
    const { form } = this.state

    const error = validateBalance(form.balance)
    await this.setState(() => ({ form: { ...form, error } }))

    // Update in firestore if necessary
    if (form.error === '' && form.balance !== client.balance) {
      // Setup the update object
      const clientUpdate = { balance: form.balance }
      firestore.update({ collection: 'clients', doc: client.id }, clientUpdate)
    }
    // Close the popover
    this.setState(() => ({ popoverOpen: false }))
  }

  handleInputChange = e => {
    e.persist()
    this.setState(prevState => ({
      ...prevState,
      form: { ...prevState.form, balance: e.target.value },
    }))
  }

  handleDeleteClient = () => {
    const { client, firestore } = this.props

    // Remove from firestore
    firestore.delete({ collection: 'clients', doc: client.id })
    this.setState(() => ({ shouldRedirect: true }))
  }

  render() {
    if (this.state.shouldRedirect) {
      return <Redirect to={HOME} />
    }

    const { isLoading, client } = this.state
    const WithLoadingClient = WithLoading(ClientDetails)

    return (
      <Fragment>
        <AppNavbarSecondary route={HOME} title="Back to Clients">
          <ClientActionsEditDelete
            editRoute={`/client/edit/${client && client.id}`}
            deleteOnClick={this.handleDeleteClient}
          />
        </AppNavbarSecondary>

        <Container>
          <Row>
            <Col>
              <Header title="Client Details" icon="user-circle">
                <Balance
                  totalOwed={client && client.balance}
                  prefix="Balance"
                  onClick={this.handlePopoverToggle}
                  id="balanceEdit"
                >
                  <FontAwesomeIcon icon="dot-circle" />
                </Balance>
                <Popover
                  placement="bottom"
                  isOpen={this.state.popoverOpen}
                  target="balanceEdit"
                  toggle={this.handlePopoverToggle}
                >
                  <PopoverHeader>Edit Current Balance</PopoverHeader>
                  <PopoverBody>
                    <BalanceEdit
                      onSubmit={this.handleFormSubmit}
                      onChange={this.handleInputChange}
                      value={client && this.state.form.balance}
                      error={client && this.state.form.error}
                    />
                  </PopoverBody>
                </Popover>
              </Header>
            </Col>
          </Row>
          <Row>
            <Col>
              <main className="mt-3">
                <WithLoadingClient loading={isLoading} client={client} />
              </main>
            </Col>
          </Row>
        </Container>
      </Fragment>
    )
  }
}

ClientPage.propTypes = {
  client: PropTypes.object,
  firestore: PropTypes.object,
}

const mapStateToProps = ({
  firestore: {
    ordered: { client },
  },
}) => {
  if (!client) return { client: {} }
  return { client: client[0] }
}

const firestoreQuery = props => [
  { collection: 'clients', storeAs: 'client', doc: props.match.params.id },
]

export default compose(
  firestoreConnect(firestoreQuery),
  connect(mapStateToProps)
)(ClientPage)
