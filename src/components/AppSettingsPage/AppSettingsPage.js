import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
// import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { Col, Container, Row } from 'reactstrap'
import AppNavbarSecondary from '../AppNavbarSecondary/AppNavbarSecondary'
import Header from '../Header/Header'
import AppSettings from '../AppSettings/AppSettings'
import {
  setAllowRegistration,
  setDisabledBalanceOnAdd,
  setDisabledBalanceOnEdit,
} from '../../actions/settings'
import { HOME } from '../../routes'

class AppSettingsPage extends Component {
  handleInputChange = e => {
    this.props[e.target.name]()
  }

  render() {
    return (
      <Fragment>
        <AppNavbarSecondary route={HOME} title="Back to Clients" />
        <Container>
          <Row>
            <Col>
              <Header title="Settings" icon="tasks" />
            </Col>
          </Row>
          <Row>
            <Col>
              <main className="mt-3">
                <AppSettings
                  settings={this.props.settings}
                  handleInputChange={this.handleInputChange}
                />
              </main>
            </Col>
          </Row>
        </Container>
      </Fragment>
    )
  }
}

AppSettingsPage.propTypes = {
  settings: PropTypes.object.isRequired,
  setDisabledBalanceOnAdd: PropTypes.func.isRequired,
  setDisabledBalanceOnEdit: PropTypes.func.isRequired,
  setAllowRegistration: PropTypes.func.isRequired,
}

const mapStateToProps = ({ firebase: { auth }, settings }) => ({
  auth,
  settings,
})

const mapDispatchToProps = {
  setAllowRegistration,
  setDisabledBalanceOnAdd,
  setDisabledBalanceOnEdit,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppSettingsPage)
