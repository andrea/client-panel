import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firebaseConnect } from 'react-redux-firebase'
import {
  Button,
  Card,
  CardBody,
  CardSubtitle,
  CardTitle,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
} from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import toastr from '../../actions/toastr'
import AlertMessage from '../AlertMessage/AlertMessage'

class Login extends Component {
  state = {
    user: {
      email: '',
      password: '',
    },
  }

  handleInputChange = e => {
    e.persist()
    this.setState(state => ({
      user: { ...state.user, [e.target.name]: e.target.value },
    }))
  }

  handleSubmit = async e => {
    e.preventDefault()
    try {
      await this.props.firebase.login(this.state.user)
    } catch ({ message }) {
      this.props.toastr(message, 'error')
    }
  }

  render() {
    return (
      <Fragment>
        <Container>
          <Row>
            <Col
              sm="12"
              md={{ size: 8, offset: 2 }}
              lg={{ size: 6, offset: 3 }}
            >
              <main className="mt-5 px-xl-5">
                <Card>
                  <CardBody>
                    <CardTitle className="text-center">
                      <FontAwesomeIcon
                        icon="user-circle"
                        size="3x"
                        className="text-primary"
                      />
                      <p className="h1 font-weight-light mt-2 text-primary">
                        User Login
                      </p>
                    </CardTitle>
                    <CardSubtitle>
                      <AlertMessage
                        message={this.props.toast.message}
                        type={this.props.toast.type}
                        isEmpty={this.props.toast.isEmpty}
                      />
                    </CardSubtitle>
                    <Form onSubmit={this.handleSubmit}>
                      <FormGroup>
                        <Label for="email">Email</Label>
                        <Input
                          type="email"
                          name="email"
                          id="email"
                          bsSize="lg"
                          value={this.state.user.email}
                          onChange={this.handleInputChange}
                        />
                      </FormGroup>
                      <FormGroup>
                        <Label for="password">Password</Label>
                        <Input
                          type="password"
                          name="password"
                          id="password"
                          bsSize="lg"
                          value={this.state.user.password}
                          onChange={this.handleInputChange}
                        />
                      </FormGroup>
                      <FormGroup>
                        <Button
                          block
                          size="lg"
                          color="primary"
                          className="mt-4"
                          type="submit"
                        >
                          Login
                        </Button>
                      </FormGroup>
                    </Form>
                  </CardBody>
                </Card>
              </main>
            </Col>
          </Row>
        </Container>
      </Fragment>
    )
  }
}

Login.propTypes = {
  firebase: PropTypes.object.isRequired,
  toast: PropTypes.object,
  toastr: PropTypes.func.isRequired,
}

const mapStateToProps = ({ toast }) => ({ toast })

const mapDispatchToProps = dispatch => ({
  toastr: (message, messageType) => dispatch(toastr(message, messageType)),
})

export default compose(
  firebaseConnect(),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Login)
