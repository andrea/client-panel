import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import { Col, Container, Row } from 'reactstrap'
import AppNavbarSecondary from '../AppNavbarSecondary/AppNavbarSecondary'
import Header from '../Header/Header'
import ClientForm from '../ClientForm/ClientForm'
import { HOME } from '../../routes'
import { validateNewClient } from '../../validation'

class ClientAdd extends Component {
  initialState = {
    client: {
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      balance: 0,
    },
    errors: {},
    shouldRedirect: false,
  }

  state = { ...this.initialState }

  handleSubmit = async e => {
    e.preventDefault()
    const errors = validateNewClient(this.state.client)
    await this.setState(() => ({ errors }))
    if (Object.keys(this.state.errors).length === 0) {
      await this.props.firestore.add(
        { collection: 'clients' },
        this.state.client
      )
      this.setState(() => ({ shouldRedirect: true }))
    }
  }

  handleReset = () => {
    this.setState(() => ({ ...this.initialState }))
  }

  handleInputChange = e => {
    e.persist()
    this.setState(state => ({
      client: { ...state.client, [e.target.name]: e.target.value },
    }))
  }

  render() {
    if (this.state.shouldRedirect) {
      return <Redirect to={HOME} />
    }

    return (
      <Fragment>
        <AppNavbarSecondary route={HOME} title="Back to Clients" />
        <Container>
          <Row>
            <Col>
              <Header title="Add Client" icon="user-circle" />
            </Col>
          </Row>
          <Row>
            <Col>
              <main className="mt-3">
                <ClientForm
                  client={this.state.client}
                  errors={this.state.errors}
                  handleSubmit={this.handleSubmit}
                  handleReset={this.handleReset}
                  handleInputChange={this.handleInputChange}
                  disableBalanceInput={this.props.settings.disableBalanceOnAdd}
                />
              </main>
            </Col>
          </Row>
        </Container>
      </Fragment>
    )
  }
}

ClientAdd.propTypes = {
  firestore: PropTypes.object.isRequired,
  settings: PropTypes.object.isRequired,
}

const mapStateToProps = ({ settings }) => ({ settings })

export default compose(
  firestoreConnect(),
  connect(mapStateToProps)
)(ClientAdd)
