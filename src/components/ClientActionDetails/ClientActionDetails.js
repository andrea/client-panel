import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Button } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const ClientActionDetails = ({ detailsRoute }) => (
  <Button tag={Link} to={detailsRoute} color="primary" size="sm" outline>
    <span className="px-2">Details</span>
    <FontAwesomeIcon icon="chevron-circle-right" />
  </Button>
)

ClientActionDetails.propTypes = {
  detailsRoute: PropTypes.string,
}

ClientActionDetails.defaultProps = {
  detailsRoute: '#',
}

export default ClientActionDetails
