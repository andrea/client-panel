import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faAt,
  faChevronCircleLeft,
  faChevronCircleRight,
  faDotCircle,
  faEdit,
  faIdCard,
  faPencilAlt,
  faPlusCircle,
  faSignInAlt,
  faTasks,
  faTimesCircle,
  faUserCircle,
} from '@fortawesome/free-solid-svg-icons'

export default () =>
  library.add(
    faAt,
    faChevronCircleLeft,
    faChevronCircleRight,
    faDotCircle,
    faEdit,
    faIdCard,
    faPencilAlt,
    faPlusCircle,
    faSignInAlt,
    faTasks,
    faTimesCircle,
    faUserCircle
  )
