import React, { Component, Fragment } from 'react'
import { Redirect } from 'react-router-dom'
import { Col, Container, Jumbotron, Row } from 'reactstrap'
import { HOME } from '../../routes'

class NotFound extends Component {
  state = {
    shouldRedirect: false,
  }

  componentDidMount() {
    this.timer = setTimeout(
      () => this.setState(() => ({ shouldRedirect: true })),
      10e3
    )
  }

  componentWillUnmount() {
    clearTimeout(this.timer)
  }

  render() {
    if (this.state.shouldRedirect) {
      return <Redirect to={HOME} />
    }

    return (
      <Fragment>
        <Container>
          <Row>
            <Col>
              <main className="mt-3">
                <Jumbotron fluid className="mb-0 bg-white">
                  <Container fluid>
                    <h1 className="display-3">404!</h1>
                    <p className="lead">
                      The page you were looking for is not here. We will try to
                      automatically redirect you in 10 seconds.
                    </p>
                    <hr className="my-2" />
                    <p>
                      Please use the main navigation menu to go back back to the
                      app home page.
                    </p>
                  </Container>
                </Jumbotron>
              </main>
            </Col>
          </Row>
        </Container>
      </Fragment>
    )
  }
}

export default NotFound
