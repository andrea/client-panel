import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { Container, Jumbotron } from 'reactstrap'
import { HOME } from '../../routes'

class ClientNotFound extends Component {
  state = {
    shouldRedirect: false,
  }

  componentDidMount() {
    this.timer = setTimeout(
      () => this.setState(() => ({ shouldRedirect: true })),
      10e3
    )
  }

  componentWillUnmount() {
    clearTimeout(this.timer)
  }

  render() {
    if (this.state.shouldRedirect) {
      return <Redirect to={HOME} />
    }

    return (
      <Jumbotron fluid className="mb-0 bg-white">
        <Container fluid>
          <h1 className="display-3">Not Found</h1>
          <p className="lead">
            The client record you were looking for does not exist. We will try
            to automatically redirect you in 10 seconds.
          </p>
          <hr className="my-2" />
          <p>
            Please use the main navigation menu to go back back to the app home
            page.
          </p>
        </Container>
      </Jumbotron>
    )
  }
}

export default ClientNotFound
