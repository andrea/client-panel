import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { Col, Container, Row } from 'reactstrap'
import AppNavbarSecondary from '../AppNavbarSecondary/AppNavbarSecondary'
import ClientList from '../ClientList/ClientList'
import WithLoading from '../WithLoading/WithLoading'
import Header from '../Header/Header'
import Balance from '../Balance/Balance'
import ClientActionAdd from '../ClientActionAdd/ClientActionAdd'
import { ADD_CLIENT } from '../../routes'

class Home extends Component {
  state = {
    isLoading: true,
    totalOwed: 0,
  }

  // eslint-disable-next-line no-unused-vars
  componentDidUpdate(props, state, snapshot) {
    if (props.clients !== this.props.clients) {
      const totalOwed = this.props.clients.reduce((a, v) => {
        if (!Number.parseFloat(v.balance)) return a
        return a + Number.parseFloat(v.balance)
      }, 0)
      this.setState(() => ({ isLoading: false, totalOwed }))
    }
  }

  render() {
    const WithLoadingClientList = WithLoading(ClientList)

    return (
      <Fragment>
        <AppNavbarSecondary>
          <ClientActionAdd addRoute={ADD_CLIENT} />
        </AppNavbarSecondary>
        <Container>
          <Row>
            <Col>
              <Header title="Clients" icon="id-card">
                <Balance
                  totalOwed={this.state.totalOwed}
                  prefix="Total Owed"
                  disabled
                />
              </Header>
            </Col>
          </Row>
          <Row>
            <Col>
              <main className="mt-3">
                <WithLoadingClientList
                  loading={this.state.isLoading}
                  clients={this.props.clients}
                />
              </main>
            </Col>
          </Row>
        </Container>
      </Fragment>
    )
  }
}

Home.propTypes = {
  clients: PropTypes.array,
  firestore: PropTypes.object.isRequired,
}

const mapStateToProps = state => ({
  clients: state.firestore.ordered.clients,
})

export default compose(
  firestoreConnect([{ collection: 'clients' }]),
  connect(mapStateToProps)
)(Home)
