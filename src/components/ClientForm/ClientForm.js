import React from 'react'
import PropTypes from 'prop-types'
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormFeedback,
  FormGroup,
  Input,
  Label,
} from 'reactstrap'

const ClientForm = ({
  client,
  errors,
  handleSubmit,
  handleReset,
  handleInputChange,
  disableBalanceInput,
}) => (
  <Card>
    <CardHeader className="text-muted" />
    <CardBody>
      <Form onSubmit={handleSubmit} onReset={handleReset} noValidate>
        <FormGroup>
          <Label for="firstName">First Name</Label>
          <Input
            type="text"
            bsSize="lg"
            name="firstName"
            id="firstName"
            placeholder="First Name"
            value={client.firstName}
            onChange={handleInputChange}
            invalid={errors.firstName}
          />
          <FormFeedback>{errors.firstName}</FormFeedback>
        </FormGroup>
        <FormGroup>
          <Label for="lastName">Last Name</Label>
          <Input
            type="text"
            bsSize="lg"
            name="lastName"
            id="lastName"
            placeholder="Last Name"
            value={client.lastName}
            onChange={handleInputChange}
            invalid={errors.lastName}
          />
          <FormFeedback>{errors.lastName}</FormFeedback>
        </FormGroup>
        <FormGroup>
          <Label for="email">Email</Label>
          <Input
            type="email"
            bsSize="lg"
            name="email"
            id="email"
            placeholder="Email"
            value={client.email}
            onChange={handleInputChange}
            invalid={errors.email}
          />
          <FormFeedback>{errors.email}</FormFeedback>
        </FormGroup>
        <FormGroup>
          <Label for="phone">Phone</Label>
          <Input
            type="text"
            bsSize="lg"
            name="phone"
            id="phone"
            placeholder="Phone"
            value={client.phone}
            onChange={handleInputChange}
            invalid={errors.phone}
          />
          <FormFeedback>{errors.phone}</FormFeedback>
        </FormGroup>
        <FormGroup>
          <Label for="balance">Balance</Label>
          <Input
            type="number"
            bsSize="lg"
            name="balance"
            id="balance"
            placeholder="Balance"
            value={client.balance}
            onChange={handleInputChange}
            invalid={errors.balance}
            disabled={disableBalanceInput}
          />
          <FormFeedback>{errors.balance}</FormFeedback>
        </FormGroup>
        <FormGroup row>
          <Col sm="6">
            <Button block type="reset" size="lg">
              Reset Form
            </Button>
          </Col>
          <Col sm="6">
            <Button color="primary" block type="submit" size="lg">
              Submit
            </Button>
          </Col>
        </FormGroup>
      </Form>
    </CardBody>
    <CardFooter className="text-muted" />
  </Card>
)

ClientForm.propTypes = {
  client: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  handleReset: PropTypes.func.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  disableBalanceInput: PropTypes.bool,
}

export default ClientForm
