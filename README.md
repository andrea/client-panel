# ClientPanel

A React/Firebase client management application, featuring:

- Full CRUD operations on client records
- User dashboard allowing updating and deleting client informations
- Firebase Authentication and Firebase/Firestore database


## Requirements

To run the application you will need to:

- [Install Node.js][Install Node] runtime environment
- [create a firebase project][Create Firebase Project]



## Quick Start

To run the application...

- Clone the repository
- Log in to your Firebase account and create a Firebase project in the Firebase console.
- Follow [Firebase web setup guide][Firebase Web Setup Guide] get your project's initialization informations.
- Open Terminal, head over the project root folder, then rename the file `example.env` to `.env`
- Edit you `.env` file and fill up the empty keys related to your GitHub OAuth App.
- From the project's root folder, install the application by running:  

```bash
npm install
```

- Start the client application by running:
  
```bash
npm start
```

Now head to `http://localhost:3000` and see your running app!

[Install Node]: https://nodejs.org/en/download/
[Create Firebase Project]: https://firebase.google.com/
[Firebase Web Setup Guide]: https://firebase.google.com/docs/web/setup
