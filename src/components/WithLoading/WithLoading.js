import React from 'react'
import Loader from 'react-loader-spinner'
import { Container, Jumbotron } from 'reactstrap'
import ClientNotFound from '../ClientNotFound/ClientNotFound'

const WithLoading = Component =>
  function WihLoadingComponent({ loading, ...props }) {
    if (!loading && props[Object.keys(props)[0]])
      return <Component {...props} />
    if (!loading && !props[Object.keys(props)[0]]) {
      return <ClientNotFound />
    }
    return (
      <Jumbotron fluid className="bg-white">
        <Container
          fluid
          className="d-flex justify-content-center align-items-center"
        >
          <Loader type="TailSpin" color="#dadada" height="64" width="64" />
        </Container>
      </Jumbotron>
    )
  }

export default WithLoading
