import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { NavLink as RouterNavLink } from 'react-router-dom'

import { NavItem, NavLink } from 'reactstrap'
import { SETTINGS } from '../../routes'

const NavLogin = ({ salutation, username, handleLogoutClick }) => (
  <Fragment>
    <NavItem>
      <NavLink>
        <span className="text-light">{salutation}</span> {username}
      </NavLink>
    </NavItem>
    <NavItem>
      <NavLink tag={RouterNavLink} to={SETTINGS} activeClassName="active">
        Settings
      </NavLink>
    </NavItem>
    <NavItem>
      <NavLink href="#!" onClick={handleLogoutClick}>
        Logout
      </NavLink>
    </NavItem>
  </Fragment>
)

NavLogin.propTypes = {
  handleLogoutClick: PropTypes.func.isRequired,
  salutation: PropTypes.string,
  username: PropTypes.string,
}

export default NavLogin
