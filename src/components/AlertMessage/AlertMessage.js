import React from 'react'
import PropTypes from 'prop-types'
import { Alert } from 'reactstrap'

const AlertMessage = ({ message, type, isEmpty }) => {
  const toast = messageType => {
    switch (messageType) {
      case 'success':
        return 'success'
      case 'error':
        return 'danger'
      default:
        return 'info'
    }
  }

  return (
    <Alert color={toast(type)} isOpen={!isEmpty} className="mt-4 mb-3">
      {message}
    </Alert>
  )
}

AlertMessage.propTypes = {
  message: PropTypes.string,
  type: PropTypes.string,
  isEmpty: PropTypes.bool,
}

AlertMessage.defaultProps = {
  message: 'I&apos;m toast!',
}

export default AlertMessage
