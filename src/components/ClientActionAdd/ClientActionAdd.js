import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Button } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const ClientActionAdd = ({ addRoute }) => (
  <Button tag={Link} to={addRoute} color="primary" outline>
    <span className="px-2">Add New Client</span>
    <FontAwesomeIcon icon="plus-circle" />
  </Button>
)

ClientActionAdd.propTypes = {
  addRoute: PropTypes.string,
}

ClientActionAdd.defaultProps = {
  addRoute: '#',
}

export default ClientActionAdd
