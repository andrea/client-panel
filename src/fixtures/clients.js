// eslint-disable-next-line import/no-extraneous-dependencies
import faker from 'faker'

faker.locale = 'en'

const userGen = () => ({
  id: faker.random.uuid(),
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  email: faker.internet.email(),
  phone: faker.phone.phoneNumber(),
  balance: faker.finance.amount(0, 1000, 0),
})

export default num => Array.from({ length: num }, () => userGen())
