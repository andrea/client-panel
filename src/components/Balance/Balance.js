import React from 'react'
import PropTypes from 'prop-types'
import { Badge, Button } from 'reactstrap'
import cx from 'classnames'
import numeral from 'numeral'

const Balance = ({ totalOwed, prefix, children, disabled, onClick, id }) => (
  <Button
    color={disabled ? 'secondary' : 'dark'}
    id={id}
    size="lg"
    outline
    disabled={disabled}
    style={{ opacity: '1' }}
    onClick={onClick}
  >
    {prefix}:
    <Badge className={cx('mx-2', totalOwed >= 0 ? 'bg-success' : 'bg-danger')}>
      {numeral(totalOwed).format('$0,0.00')}
    </Badge>
    {children}
  </Button>
)

Balance.propTypes = {
  totalOwed: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  prefix: PropTypes.string,
  children: PropTypes.object,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  id: PropTypes.string,
}

Balance.defaultProps = {
  totalOwed: 0,
  prefix: 'Total Owed',
  disabled: false,
}

export default Balance
