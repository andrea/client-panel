import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Button, ButtonGroup } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const ClientActionsEditDelete = ({ editRoute, deleteOnClick }) => (
  <ButtonGroup>
    <Button tag={Link} to={editRoute} color="primary" outline>
      <span className="px-2">Edit</span>
      <FontAwesomeIcon icon="dot-circle" />
    </Button>
    <Button onClick={deleteOnClick} to="#" color="primary" outline>
      <span className="px-2">Delete</span>
      <FontAwesomeIcon icon="times-circle" />
    </Button>
  </ButtonGroup>
)

ClientActionsEditDelete.propTypes = {
  editRoute: PropTypes.string,
  deleteOnClick: PropTypes.func,
}

ClientActionsEditDelete.defaultProps = {
  editRoute: '#',
}

export default ClientActionsEditDelete
