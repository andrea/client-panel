import { NOTIFY_USER } from '../actions/types'

const initialState = {
  message: null,
  messageType: null,
  isEmpty: true,
}

const toast = (state = initialState, action) => {
  switch (action.type) {
    case NOTIFY_USER:
      return {
        ...state,
        ...action.payload,
      }
    default:
      return state
  }
}

export default toast
