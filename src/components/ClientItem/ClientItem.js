import React from 'react'
import PropTypes from 'prop-types'
import numeral from 'numeral'
import ClientActionDetails from '../ClientActionDetails/ClientActionDetails'
import { CLIENT } from '../../routes'

const ClientItem = ({ client }) => (
  <tr key={client.id}>
    <td>{`${client.firstName} ${client.lastName}`}</td>
    <td className="d-none d-sm-table-cell">{client.email}</td>
    <td className="text-right">{numeral(client.balance).format('$0,0.00')}</td>
    <td className="text-right">
      <ClientActionDetails detailsRoute={`${CLIENT}/${client.id}`} />
    </td>
  </tr>
)

ClientItem.propTypes = {
  client: PropTypes.object.isRequired,
}

export default ClientItem
