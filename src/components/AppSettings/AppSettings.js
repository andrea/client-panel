import React from 'react'
import PropTypes from 'prop-types'

import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Form,
  FormGroup,
  Input,
  Label,
} from 'reactstrap'

const AppSettings = ({
  settings: { disableBalanceOnAdd, disableBalanceOnEdit, allowRegistration },
  handleInputChange,
}) => (
  <Card>
    <CardHeader className="text-muted" />
    <CardBody>
      <Form>
        <FormGroup check>
          <Label check>
            <Input
              type="checkbox"
              checked={allowRegistration}
              name="setAllowRegistration"
              onChange={handleInputChange}
            />
            <span className="ml-2">Allow Registration</span>
          </Label>
        </FormGroup>
        <FormGroup check>
          <Label check>
            <Input
              type="checkbox"
              checked={disableBalanceOnAdd}
              name="setDisabledBalanceOnAdd"
              onChange={handleInputChange}
            />
            <span className="ml-2">Disable Balance on Add</span>
          </Label>
        </FormGroup>
        <FormGroup check>
          <Label check>
            <Input
              type="checkbox"
              checked={disableBalanceOnEdit}
              name="setDisabledBalanceOnEdit"
              onChange={handleInputChange}
            />
            <span className="ml-2">Disable Balance on Edit</span>
          </Label>
        </FormGroup>
      </Form>
    </CardBody>
    <CardFooter className="text-muted" />
  </Card>
)

AppSettings.propTypes = {
  settings: PropTypes.object,
  handleInputChange: PropTypes.func,
}

export default AppSettings
