import { NOTIFY_USER } from './types'

const toastr = (message, type) => ({
  type: NOTIFY_USER,
  payload: {
    message,
    type,
    isEmpty: false,
  },
})

export default toastr
